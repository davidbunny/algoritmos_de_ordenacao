package algoritmos;

public class heap {

	public static void heapSort(int[] vetor) {
		construirMaxHeap(vetor);
        int n = vetor.length;                          // n
        for (int i = vetor.length - 1; i > 0; i--) {   // n ^2 
            swap(vetor, i, 0);                           
            maxHeap(vetor, 0, --n);
        }
			
	}

	 private static void construirMaxHeap(int[] vetor) {
	        for (int i = vetor.length / 2 - 1; i >= 0; i--)    // n^2 
	            maxHeap(vetor, i, vetor.length);

	    }

	     private static void maxHeap(int[] vetor, int pos, int tamanhoDoVetor)  
	     {  

	         int max = 2 * pos + 1, right = max + 1;        // n
	         if (max < tamanhoDoVetor)  
	         {  

	             if (right < tamanhoDoVetor && vetor[max] < vetor[right])   //n
	                 max = right;

	             if (vetor[max] > vetor[pos])   //n 
	             {  
	                 swap(vetor, max, pos);  
	                 maxHeap(vetor, max, tamanhoDoVetor);  
	             }  
	         }  
	     }

	    public static void swap(int[] v, int j, int aposJ) {
	        int aux = v[j];       //n
	        v[j] = v[aposJ];      //n 
	        v[aposJ] = aux;       //n 
	    }
			
}
