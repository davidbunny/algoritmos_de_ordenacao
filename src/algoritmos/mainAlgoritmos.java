package algoritmos;
import java.util.Random;

public class mainAlgoritmos {

	public static void main(String[] args) {
		
		int[] vetor = new int[1000];
		Random random = new Random();
		
		System.out.println("Preparando vetor...");
		for(int i = 0; i < vetor.length;i++){   
			vetor[i] = random.nextInt(vetor.length);	
		}

		System.out.println("Rodando Algoritmos...");
				
		// Insertion Sort
		System.out.println("Insertion Sort");
		double tempoInicial = System.currentTimeMillis();
		insertion.insertionSort(vetor);
		double tempoFinal = System.currentTimeMillis();
		System.out.println( tempoFinal - tempoInicial + "ms" );System.out.println("----");
		
		// Selection Sort
		System.out.println("Selection Sort");
		tempoInicial = System.currentTimeMillis();
		selection.selectionSort(vetor);
		tempoFinal = System.currentTimeMillis();
		System.out.println( tempoFinal - tempoInicial + "ms" );System.out.println("----");
		
		//Bubble sort
		System.out.println("Bubble Sort");
		tempoInicial = System.currentTimeMillis();
		bubble.bubbleSort(vetor);
		tempoFinal = System.currentTimeMillis();
		System.out.println( tempoFinal - tempoInicial + "ms" );System.out.println("----");
		
		//Shell sort
		System.out.println("Shell Sort");
		tempoInicial = System.currentTimeMillis();
		shell.shellSort(vetor);
		tempoFinal = System.currentTimeMillis();
		System.out.println( tempoFinal - tempoInicial + "ms" );System.out.println("----");
		
		// Heap Sort
	
	}

	
}
