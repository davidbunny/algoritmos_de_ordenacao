package algoritmos;

public class merge {

	public static int[] mergeSort(int[] vetor) {

		if (vetor.length <= 1) { // chave para parar a recursividade
			return vetor;
		}

		int meio = vetor.length / 2; // calcula a metade do vetor
		int[] dir = vetor.length % 2 == 0 ? new int[meio] : new int[meio + 1]; // instancia para
																				// elementos da direita
		int[] esq = new int[meio]; // instancia vetor para elementos da esquerda

		int[] aux = new int[vetor.length]; // vetor auxiliar para trocas

		for (int i = 0; i < meio; i++) { // preenche vetor esq com elemntos at�
											// a metado do vetor original
			esq[i] = vetor[i];
		}

		int auxIndex = 0;
		for (int i = meio; i < vetor.length; i++) {
			dir[auxIndex] = vetor[i]; // preenche vetor dir com elementos a partir da metade do vetor original
			auxIndex++;
		}

		esq = mergeSort(esq); // chama a fun��o recursivamente para realizar as
								// opera��es novamente.
		dir = mergeSort(dir);

		aux = mergeVetor(esq, dir); // Faz a ordena��o das partes e jun�ao.

		return aux;
	}

	static int[] mergeVetor(int[] esq, int[] dir) {
		int[] aux = new int[esq.length + dir.length];

		int iDir = 0, iEsq = 0, iAux = 0;

		while (iEsq < esq.length || iDir < dir.length) { // ordena at� o ultimo
															// elemento dos
															// vetores
			if (iEsq < esq.length && iDir < dir.length) {
				if (esq[iEsq] <= dir[iDir]) {
					aux[iAux] = esq[iEsq];
					iAux++;
					iEsq++;
				} else {
					aux[iAux] = dir[iDir];
					iAux++;
					iDir++;
				}
			} else if (iEsq < esq.length) {
				aux[iAux] = esq[iEsq];
				iAux++;
				iEsq++;
			} else if (iDir < dir.length) {
				aux[iAux] = dir[iDir];
				iAux++;
				iDir++;
			}
		}
		return aux;
	}

}
