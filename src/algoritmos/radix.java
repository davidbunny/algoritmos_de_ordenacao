package algoritmos;

public class radix {

	public static void radixSort(int[] vetor) {
	   
	    for (int shift = Integer.SIZE - 1; shift > -1; shift--) {
	      
	        int[] aux = new int[vetor.length];
	     
	        int j = 0;
	 	    
	        for (int i = 0; i < vetor.length; i++) {
	     
	            boolean move = vetor[i] << shift >= 0;
	 
	            if (shift == 0 ? !move : move) {
	                aux[j] = vetor[i];
	                j++;
	            } else {
	           
	                vetor[i - j] = vetor[i];
	            }
	        }
	 
	    
	        for (int i = j; i < aux.length; i++) {
	            aux[i] = vetor[i - j];
	        }
	 	 
	        vetor = aux;
	    }
	 
	  
	}
}
