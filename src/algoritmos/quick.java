package algoritmos;

public class quick {

	  public static void quickSort(int[] vetor, int inicio, int fim) 
	    {
	    
	        int middle = inicio + (fim - inicio) / 2;   //n
	        int pivot = vetor[middle];   //n
	 	      
	        int i = inicio, j = fim;   //n
	        while (i <= j)              
	        {
	         
	            while (vetor[i] < pivot) 
	            {
	                i++;
	            }
	          
	            while (vetor[j] > pivot) 
	            {
	                j--;
	            }
	            
	            if (i <= j) 
	            {
	                swap (vetor, i, j);
	                i++;
	                j--;
	            }
	        }
	      
	        if (inicio < j){
	            quickSort(vetor, inicio, j);
	        }
	        if (fim > i){
	            quickSort(vetor, i, fim);
	        }
	    }
	     
	    public static void swap (int vetor[], int x, int y)
	    {
	        int temp = vetor[x];
	        vetor[x] = vetor[y];
	        vetor[y] = temp;
	    }
}
	

