package algoritmos;
import java.util.Random;

public class mainAlgoritmos_2 {

	public static void main(String[] args) {
		
		int[] vetor = new int[2];
		Random random = new Random();
		
		System.out.println("Preparando vetor...");
		for(int i = 0; i < vetor.length;i++){   
			vetor[i] = random.nextInt(vetor.length);	
		}

		System.out.println("Rodando Algoritmos...");
		/*		
		// Heap Sort
		System.out.println("Heap Sort");
		long tempoInicial = System.currentTimeMillis();
		heap.heapSort(vetor);
		long tempoFinal = System.currentTimeMillis();
		System.out.println( tempoFinal - tempoInicial + "ms" );System.out.println("----");
		
		// Quick Sort  /*
		System.out.println("Quick Sort");
		int inicio = 0;
		int fim = vetor.length - 1;
	    tempoInicial = System.currentTimeMillis();
		quick.quickSort(vetor, inicio, fim);
		tempoFinal = System.currentTimeMillis();
		System.out.println( tempoFinal - tempoInicial + "ms" );System.out.println("----");
		*/
		// Merge Sort 
		System.out.println("Merge Sort");
		long tempoInicial = System.currentTimeMillis();
		merge.mergeSort(vetor);
		long tempoFinal = System.currentTimeMillis();
		System.out.println( tempoFinal - tempoInicial + "ms" );System.out.println("----");
		/*		
		// Radix Sort
		System.out.println("Radix Sort");
		tempoInicial = System.currentTimeMillis();
	    radix.radixSort(vetor);
		tempoFinal = System.currentTimeMillis();
		System.out.println( tempoFinal - tempoInicial + "ms" );System.out.println("----");
		*/
		
	}

	
}
